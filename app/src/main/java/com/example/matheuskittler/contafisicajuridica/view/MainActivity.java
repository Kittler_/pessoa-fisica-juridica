package com.example.matheuskittler.contafisicajuridica.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.example.matheuskittler.contafisicajuridica.R;
import com.example.matheuskittler.contafisicajuridica.adapter.AccountAdapter;
import com.example.matheuskittler.contafisicajuridica.model.Account;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

	private AccountAdapter adapter;
	private RecyclerView rvPerson;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		rvPerson = findViewById(R.id.rvPerson);

		List<Account> accounts = new ArrayList<>();

		for (int i = 10; i > 0; i--) {
			accounts.add(new Account("Usuario = " + String.valueOf(i),
					"CL", "000.000.000-10",
					i % 2 == 0));
		}

		adapter = new AccountAdapter(this, accounts);
		rvPerson.setAdapter(adapter);

	}
}
