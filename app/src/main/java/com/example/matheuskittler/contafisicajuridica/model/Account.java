package com.example.matheuskittler.contafisicajuridica.model;


/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 27/02/2019$
 */
public class Account {

	private String name;
	private String address;
	private String document;
	private boolean isJuridic;

	public Account(String name, String address, String document, boolean isJuridic) {
		this.name = name;
		this.address = address;
		this.document = document;
		this.isJuridic = isJuridic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	public boolean isJuridic() {
		return isJuridic;
	}

	public void setJuridic(boolean juridic) {
		isJuridic = juridic;
	}
}
