package com.example.matheuskittler.contafisicajuridica.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.matheuskittler.contafisicajuridica.R;
import com.example.matheuskittler.contafisicajuridica.model.Account;
import com.example.matheuskittler.contafisicajuridica.view.AccountViewHolder;
import com.example.matheuskittler.contafisicajuridica.view.JuridicViewHolder;

import java.util.List;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 27/02/2019$
 */
public class AccountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

	private Context context;
	private List<Account> accounts;
	private LayoutInflater inflater;

	public AccountAdapter(Context context, List<Account> accounts) {
		this.context = context;
		this.accounts = accounts;
		this.inflater = LayoutInflater.from(context);
	}


	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
		View view;
		if (viewType == 0) {
			return new AccountViewHolder
					(inflater.inflate(R.layout.row_person, viewGroup, false));
		} else {
			return new JuridicViewHolder
					(inflater.inflate(R.layout.row_person_juridic, viewGroup, false));
		}

	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

		Account a = accounts.get(position);

//
		if (viewHolder instanceof AccountViewHolder) {

			AccountViewHolder accountViewHolder = (AccountViewHolder) viewHolder;

			accountViewHolder.tvName.setText(a.getName());
			accountViewHolder.tvNumber.setText(a.getDocument());
			accountViewHolder.tvNy.setText(a.getAddress());

		} else if (viewHolder instanceof JuridicViewHolder) {

			JuridicViewHolder juridicViewHolder = (JuridicViewHolder) viewHolder;

			juridicViewHolder.tvNameJuridic.setText(a.getName());
			juridicViewHolder.tvNy.setText(a.getAddress());
			juridicViewHolder.tvNumber.setText(a.getDocument());
		}
	}

	@Override
	public int getItemCount() {
		return accounts.size();
	}

	@Override
	public int getItemViewType(int position) {
		return accounts.get(position).isJuridic() ? 1 : 0;
		//operador ternario, não tem tantas possibilidades quanto o if por ser simples
	}
}
