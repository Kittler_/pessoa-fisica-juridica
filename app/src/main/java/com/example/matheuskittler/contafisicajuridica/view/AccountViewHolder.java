package com.example.matheuskittler.contafisicajuridica.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.matheuskittler.contafisicajuridica.R;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 28/02/2019$
 */
public class AccountViewHolder extends RecyclerView.ViewHolder {

	public TextView tvTypeAccount;
	public TextView tvName;
	public TextView tvNy;
	public TextView tvNumber;

	public AccountViewHolder(@NonNull View itemView) {
		super(itemView);

		tvName = itemView.findViewById(R.id.tvName);
		tvNy = itemView.findViewById(R.id.tvNy);
		tvTypeAccount = itemView.findViewById(R.id.tvTypeAccount);
		tvNumber = itemView.findViewById(R.id.tvNumber);

	}
}
