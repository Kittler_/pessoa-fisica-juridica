package com.example.matheuskittler.contafisicajuridica.view;

import android.support.annotation.NonNull;;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.matheuskittler.contafisicajuridica.R;

/**
 * @author Matheus Kittler
 * @author matheus.santos@operacao.rcadigital.com.br
 * @since 28/02/2019$
 */
public class JuridicViewHolder extends RecyclerView.ViewHolder {

	public TextView tvNameJuridic;
	public TextView tvNy;
	public TextView tvNumber;
	public TextView tvTypeJuridic;

	public JuridicViewHolder(@NonNull View itemView) {
		super(itemView);

		tvNameJuridic = itemView.findViewById(R.id.tvNameJuridic);
		tvNumber = itemView.findViewById(R.id.tvNumberJuridic);
		tvNy = itemView.findViewById(R.id.tvNyJuridic);
		tvTypeJuridic = itemView.findViewById(R.id.tvTypeJuridic);

	}
}
